# Arduino Pulse Counter

A short Arduino sketch using interrupts to count square pulses. 

Accurate to about 1%, tested up to 140kHz. It can likely go faster, but will get bogged down trying to handle pulses and output text over the serial connection. 

There's no external hardware to speak of, just connect your signal to the input pin and ground to ground. Your signal may expect a termination though.
