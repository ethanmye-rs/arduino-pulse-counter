/*
* A short Arduino sketch using interrupts to count square pulses. 
*
* Accurate to about 1%, tested up to 140kHz. It can likely go faster,
* but will get bogged down trying to handle pulses and output text over
* the serial connection. 
*
* Ethan Myers 2020
*/

#include <Arduino.h>

const int input = 2; //pick a pin with ISRs available on it -- pin 2 for the Uno.
unsigned long int counter = 0;
unsigned int interval = 1000;
unsigned long time;

void PulseEvent();

//In my testing, I was off by ~9 counts per 1KHz. I used a correction factor of 0.991 to fix this. 

void setup(){
  attachInterrupt(digitalPinToInterrupt(input), PulseEvent, RISING);
  time = millis();
  Serial.begin(115200);
  Serial.println(F("Start Pulse Counting")); //F macro to save some RAM!
}

void loop(){
  if(millis() - time > interval){
    Serial.println(counter * 0.991);
    counter = 0;
    time = millis();
  }
}

void PulseEvent(){
  counter++;
}
